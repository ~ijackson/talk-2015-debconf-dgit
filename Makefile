
SLIDES+= title
SLIDES+= manpage
SLIDES+= data-flow
SLIDES+= nmu-linear
SLIDES+= nmu-basic
SLIDES+= nmu-compare
SLIDES+= data-flow
SLIDES+= dflow-equal
SLIDES+= fmt-status
SLIDES+= data-flow
SLIDES+= dflow-equal
SLIDES+= manpage
SLIDES+= manpage-clean
SLIDES+= future
SLIDES+= moreinfo

SLIDEFILES=$(addsuffix .ps, $(SLIDES))

o= >$@.new && mv -f $@.new $@

all:	slides.pdf talk.ps

%.ps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 <$@.1 $o

%.eps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L eps <$@.1 $o

manpage.ps: dgit.1-manpage Makefile
	man -Tps -l $< >$@.1
	pstops -pa3 '100:0@1.7(-4cm,-47cm)' <$@.1 >$@.2
	ps2pdf -sPAPERSIZE=a3 $@.2 $@.3
	pdfcrop --bbox '0 0 841 595' $@.3 $@.4
	pdf2ps $@.3 $@.4
	pstops -pa3 '0L(21cm,0cm)' <$@.4 >$@.5
	pstops -pa4 '0(0cm,0cm)' <$@.5 >$@

manpage-clean.ps: dgit.1-manpage Makefile
	man -Tps -l $< >$@.1
	pstops -pa3 '100:3@1.7(-4cm,-43cm)' <$@.1 >$@.2
	ps2pdf -sPAPERSIZE=a3 $@.2 $@.3
	pdfcrop --bbox '0 0 841 595' $@.3 $@.4
	pdf2ps $@.3 $@.4
	pstops -pa3 '0L(21cm,0cm)' <$@.4 >$@.5
	pstops -pa4 '0(0cm,0cm)' <$@.5 >$@

nmu-compare.ps: my-history-inc.eps

%.txt.eps: %.txt ./txt2ps
	./txt2ps <$< |ps2eps -s a3 $o

%.ps:	%.lout
	lout $< $o

slides.ps: $(SLIDEFILES) Makefile
	cat $(SLIDEFILES) $o

slides.pdf:     slides.ps Makefile
	ps2pdf $< $@

talk.ps: talk.txt
	a2ps -1 -o $@ -B talk.txt

install: slides.pdf talk.txt
	rsync -vP $^ ijackson@chiark:public-html/2015/debconf-dgit-talk/
	git push ijackson@chiark:public-git/talk-2015-debconf-dgit.git
